package Servlet;

import lrodgersEcommerce.HibernateUtils;
import lrodgersEcommerce.userLogin;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "loginServlet", urlPatterns = {"/loginServlet"})
public class loginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("Nothing here");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String returnMessage = "fail";

        if (request.getParameter("username") != null && request.getParameter("password") != null) {
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            System.out.println(username);
            System.out.println(password);

            // Run code here to check if in database

            HibernateUtils.DAO checkDatabase = new HibernateUtils.DAO();
            userLogin loginCheck = new userLogin();

            loginCheck = checkDatabase.verifyUser(username, password);

            if (loginCheck != null)
                returnMessage = "id:"+loginCheck.getId()
                        +"\nusername:" + loginCheck.getUsername();
        }

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println(returnMessage);









    }
}
