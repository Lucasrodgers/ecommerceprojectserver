package lrodgersEcommerce;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;


public class HibernateUtils {


    private static final SessionFactory sessionFactory = buildSessionFactory();

    private static SessionFactory buildSessionFactory() {
        try {

            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                    .configure("hibernate.cfg.xml").build();

            Metadata metadata = new MetadataSources(serviceRegistry).getMetadataBuilder().build();
            return metadata.getSessionFactoryBuilder().build();

        } catch (Throwable e) {

            System.err.println("Initial Session Factory creation failed." + e);
            throw new ExceptionInInitializerError(e);
        }
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }


    public static class DAO {

        SessionFactory factory;
        Session session = null;

        private static DAO single_instance = null;

        public DAO() {
            factory = HibernateUtils.getSessionFactory();
        }

        public static DAO getInstance() {
            if (single_instance == null) {
                single_instance = new DAO();
            }

            return single_instance;
        }

        //This function gets the books from the database
        public userLogin verifyUser(String username, String password) {

            try {
                session = factory.openSession();
                session.getTransaction().begin();

                String sqlUserData = "from lrodgersEcommerce.userLogin where username=:username AND password=:password";


                userLogin c = (userLogin) session.createQuery(sqlUserData)
                        .setParameter("username", username)
                        .setParameter("password", password)
                        .getSingleResult();

                session.getTransaction().commit();
                return c;


            } catch (Exception e) {
                e.printStackTrace();

                // Rollback in case of an error occurred.
                session.getTransaction().rollback();
                return null;
            } finally {
                session.close();
            }
        }

/*        public userLogin verifyPassword(String password) {

            try {
                session = factory.openSession();
                session.getTransaction().begin();

                String sqlPassword = "from lrodgersEcommerce.userLogin where username=" + password;
                userLogin c = (userLogin)session.createQuery(sqlPassword).getSingleResult();
                session.getTransaction().commit();
                return c;

            } catch (Exception e) {
                e.printStackTrace();

                // Rollback in case of an error occurred.
                session.getTransaction().rollback();
                return null;
            } finally {
                session.close();
            }
        }*/

    }


    public static void shutdown() {
        getSessionFactory().close();
    }

}

